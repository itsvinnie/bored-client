import { Component } from '@angular/core';
import { ActivityService } from '../activity.service';
import { IActivity } from '../activity';

@Component({
  selector: 'app-activity-random',
  templateUrl: './activity-random.component.html',
  styleUrls: ['./activity-random.component.scss'],
})
export class ActivityRandomComponent {
  title = 'Welcome';
  activity: IActivity | null;
  errorMessage = '';
  hint: '';

  _priceFilter = '';
  get priceFilter(): string {
    return this._priceFilter;
  }
  set priceFilter(value: string) {
    this._priceFilter = value;
  }

  _participantsFilter = '';
  get participantsFilter(): string {
    return this._participantsFilter;
  }
  set participantsFilter(value: string) {
    this._participantsFilter = value;
  }

  getActivity(): void {
    this.activityService
      .getActivity(this.priceFilter, this.participantsFilter)
      .subscribe({
        next: (activity) => {
          this.activity = activity;
          this.hint = '';
          this.errorMessage = '';
        },
        error: (err) => {
          console.log(err);
          this.activity = null;
          this.errorMessage = err.message;
          if (err.hint) this.hint = err.hint;
        },
      });
  }

  constructor(private activityService: ActivityService) {}
}
