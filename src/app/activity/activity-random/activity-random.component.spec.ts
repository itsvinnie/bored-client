import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityRandomComponent } from './activity-random.component';

describe('ActivityRandomComponent', () => {
  let component: ActivityRandomComponent;
  let fixture: ComponentFixture<ActivityRandomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivityRandomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityRandomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
