import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { IActivity } from './activity';

@Injectable({
  providedIn: 'root',
})
export class ActivityService {
  private activityUrl = 'http://127.0.0.1:8000/';

  constructor(private http: HttpClient) {}

  getActivity(price = null, participants = null): Observable<IActivity> {
    const params = {};
    if (price) params['price'] = price;
    if (participants) params['participants'] = participants;
    return this.http
      .get<IActivity>(this.activityUrl, { params })
      .pipe(
        tap((data) => console.log('Activity: ' + JSON.stringify(data))),
        catchError(this.handleError)
      );
  }

  postActivity(activity: IActivity): Observable<IActivity> {
    return this.http.post<IActivity>(this.activityUrl, activity).pipe(
      tap((data) =>
        console.log('Newly created activity: ' + JSON.stringify(data))
      ),
      catchError(this.handleError)
    );
  }

  private handleError(err: HttpErrorResponse) {
    const errorObject = { ...err.error };
    if (err.error instanceof ErrorEvent) {
      errorObject['message'] = `An error occurred: ${err.error.message}`;
    } else {
      errorObject[
        'message'
      ] = `Server returned code: ${err.status}, error message is: ${err.message}`;

      if (err.status === 404)
        errorObject['hint'] = 'No activity found. Try modifying some filters.';
    }
    return throwError(errorObject);
  }
}
