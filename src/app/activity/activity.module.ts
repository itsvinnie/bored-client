import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivityRandomComponent } from './activity-random/activity-random.component';
import { ActivityNewComponent } from './activity-new/activity-new.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [ActivityRandomComponent, ActivityNewComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: '', redirectTo: 'random', pathMatch: 'full' },
      { path: 'random', component: ActivityRandomComponent },
      {
        path: 'new',
        component: ActivityNewComponent,
      },
    ]),
    FormsModule,
  ],
})
export class ActivityModule {}
