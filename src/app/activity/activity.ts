export type TTypes =
  | 'education'
  | 'recreational'
  | 'social'
  | 'diy'
  | 'charity'
  | 'cooking'
  | 'relaxation'
  | 'music'
  | 'busywork';

export const TYPES = [
  'education',
  'recreational',
  'social',
  'diy',
  'charity',
  'cooking',
  'relaxation',
  'music',
  'busywork',
];

export interface IActivity {
  activity: string;
  accessibility: string;
  type: TTypes;
  participants: number;
  price: number;
  link?: string;
}
