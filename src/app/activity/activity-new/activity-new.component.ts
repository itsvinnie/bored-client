import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivityService } from '../activity.service';
import { IActivity, TYPES } from '../activity';

@Component({
  selector: 'app-activity-new',
  templateUrl: './activity-new.component.html',
  styleUrls: ['./activity-new.component.scss'],
})
export class ActivityNewComponent {
  title = 'Add an activity';
  errorMessage = '';
  successMessage: any;
  activity: IActivity | undefined;
  _TYPES = TYPES;

  constructor(private activityService: ActivityService) {}

  createActivity(saveForm: NgForm): void {
    if (saveForm && saveForm.valid) {
      const newActivity = { ...saveForm.form.value };
      console.log(newActivity);
      this.activityService.postActivity(newActivity).subscribe({
        next: (activity) => {
          this.activity = activity;
          saveForm.reset();
          console.log('new activity: ', activity);
          this.successMessage = `Your activity "${activity.activity}" has been created. Thank you for contributing to this website!`;
        },
        error: (err) => (this.errorMessage = err.message),
      });
    } else {
      this.errorMessage = 'Please fill in the required fields.';
    }
  }
}
