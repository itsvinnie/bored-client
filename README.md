# Introduction

I was given this exercise during a job interview. At that time, I knew neither Angular nor Python so the goal was to test me and see how I handle learning new things.

The README available at https://gitlab.com/itsvinnie/bored-server explains in detail how to install and run the project locally.

Below are the instructions, in French.

# Bored Webapp

Confinement oblige, les gens s'ennuie ferme chez eux. Il faut les aider à trouver des idées d'activités. Nous allons donc créer une base de données consultable via une petite webapp pour combattre l'ennui.

Pour créer cette webapp, nous allons utiliser comme base [BoredApi](www.boredapi.com) qui va pouvoir nous donner des permières idées.

# Ce qui est attendu

## Back-end

- [ ] Une Base de Données (choix de techno libre, postgresql est ce qu'on utilise)
- [ ] Le contenu de la base de données doit être boredApi, mais peut être étendu par nos utilisateurs
- [ ] Une API REST pour consulter/remplir la DB

## Front-end

- [ ] Une page pour avoir une idée tirées au hasard, Mettre des filtres sur le nombre de personnes ou le prix est un plus
- [ ] Une page pour ajouter une nouvelle idée d'activité, on doit pouvoir la retrouver directement, aucune validation n'est nécessaire
- [ ] Un design simple mais clair, l'application doit pouvoir être vu sur mobile

# Technologie à utiliser

L'api doit être écrite en [Django](https://www.djangoproject.com/) avec une base de données de type SQL
Cette webapp devra être écrite en [Angular](https://angular.io/), le style sera fait en [SASS](https://sass-lang.com/).
La structure du projet, l'UI, l'UX sont libres, le but de la webapp est qu'elle soit fonctionnel et que le code soit propre.

# Comment rendre le projet

Le projet devra être mis sur un repository en ligne (Gitlab, Github ou autre service similaire), il doit il y avoir un README mis en place pour expliquer les choix effectués, les librairies utilisées, comment lancer l'application en local et des idées d'amélioration.
Il n'est pas demandé de prévoir le déploiement de l'application en ligne.

